# cardgame-project : Projet de support à la formation "XXX"

## Présentation générale

Ce projet est destiné à illustrer une formation de montée en compétence en java.

Ce projet illuste :

- l'utilisation de git
- l'utilisation de maven (v3 utilisée pendant l'élaboration)
- l'utilisation de sonar (version xxx utilisée)
- la création d'un projet "from scratch" de jeu de carte en java, en partant d'une idée simple de base...
- une amélioration progressive d'un code à l'origine "jettable" vers une version maintenable, propre et évolutive pouvant servir de socle à d'autres jeux.

Les travaux pratiques qui suivent introduisent ces différents outils / concepts, étape par étape.

## Pré-requis

Avoir :

- java installé (jdk 1.6 utilisé pendant l'élaboration)
	- http://www.oracle.com/technetwork/java/index.html
- maven installé et opérationnel
	- http://maven.apache.org/
- un éditeur de code java installé et opérationnel
	- ...
- git installé et opérationnel (ligne de commande)
	- http://msysgit.github.com/

## Travaux pratiques

### Contexte fictif

Vous êtes employé de la société Zonga (zonga.com) et vous souhaitez créer un jeu de carte en ligne "SupaPuker" mettant en jeux des concepts de gameplay hautements addictifs. Vous décidez de vous lancer dans la création d'un prototype d'application, afin de présenter à votre supérieur hiérarchique le projet pour obtenir son aval et pouvoir monter une équipe pour concrétiser votre idée !

### Première version du jeu : Les règles
Pour commencer simplement, vous décidez de réaliser le jeu suivant :

- Deux joueurs : chaque joueur a 20 points de "vie".
- Un jeu commun au deux joueurs, de 32 cartes numérotées, constitué de 16 cartes bleues et rouges, chaque couleur déclinée en :

	- 4 cartes de 1
	- 3 de 2
	- 2 de 3
	- et 1 de 4,5,6,7,8,9,10

- Premier tour : chaque joueur prend 3 cartes (chacun tire une carte l'un après l'autre)
- Tours suivants : chaque joueur choisit une carte dans sa main, soit bleue, soit rouge.

	- carte rouge : attaque
	- carte bleue : défense
	- valeur : nombre de points de l'attaque ou de la défense.

	Une fois les cartes choisies, chaque joueur perds autant de vie que
		(points d'attaque posés par l'adversaire - points de défense posés)

	Si personne n'attaque (deux cartes bleues choisies), personne ne perds de points de vie.

	Les cartes sont alors mises dans la défausse, chaque joueur retire une carte de la pile et l'on reprend un nouveau tour.

- Fin de la partie : quand la pile est vide, ou que l'un des joueurs arrive à 0 points de vie.

	- Si la partie se termine et que les deux joueurs ont le même nombre de points de vie : EX-AEQUO
	- Sinon, le gagnant et celui ayant le plus de points de vie.

#### Création de la structure du projet

Nous allons travailler principalement en ligne de commande. N'utilisez pas d'éditeur de code pour lancer les commandes maven ou git !

0. Créez sur votre disque dur un répertoire qui va contenir les sources du projet, par exemple **c:\\cardgame-project** (ce répertoire sera désigné par $PROJ$ dans la suite du texte)

1. Créez le socle technique de votre projet avec maven

	1. Choisissez avec soin un **groupId** et un **artifactId**
		- Hint : groupId = nom de domaine de votre société, inversé (sqli.com -> com.sqli) et artifactId = nom de votre projet
	2. La version de lancement du projet est **0.1-SNAPSHOT**
	3. En ligne de commande, placez vous dans votre répertoire $PROJ$
	4. En remplacant les XXXX dans la commande maven qui suit par les bonnes valeurs, exécutez 

		`mvn -DarchetypeGroupId=org.codehaus.mojo.archetypes -DarchetypeArtifactId=pom-root -DarchetypeVersion=1.1 -DarchetypeRepository=http://repo.maven.apache.org/maven -DgroupId=XXXX -DartifactId=XXXX -Dversion=0.1-SNAPSHOT -Darchetype.interactive=false --batch-mode archetype:generate`

		Cette commande demande à Maven de créer une structure de projet de type 'parent-pom', en utilisant le groupId et l'artifactId spécifié. Un répertoire au nom identique à l'artifactId va être créé dans $PROJ$, qui va contenir le pom-parent de notre projet.

	5. Editez le fichier 'pom.xml' du projet nouvellement créé pour y ajouter une description (tag <description></description>).

2. En utilisant la console 'git' (git bash), en ligne de commande, 'gitifier' votre répertoire $PROJ$ avec la commande

	`git init`

3. Cette commande crée un repository git (le répertoire caché .git) dans le répertoire où elle est exécutée. Le repository est au départ vide et le contenu du répertoire de travail ou il a été créé doit y être ajouté ultérieurement. Afficher l'état de votre index de commit git avec :

	`git status`

	Vous devriez avoir quelque chose de similaire :

		$ git status
		# On branch master
		#
		# Initial commit
		#
		# Untracked files:
		#   (use "git add <file>..." to include in what will be committed)
		#
		#       xxxxxxxxxxxx
		# nothing added to commit but untracked files present (use "git add" to track)

4. Ajouter maintenant a votre index de commit les fichiers non versionnés ('Untracked files', en rouge) avec 

	`git add .`

5. Vérifiez à nouveau l'état de votre répertoire de travail par rapport à l'index de commit avec
	
	`git status`

		$ git status
		# On branch master
		#
		# Initial commit
		#
		# Changes to be committed:
		#   (use "git rm --cached <file>..." to unstage)
		#
		#       new file:   xxxxxxxxxxxx
		#       (etc...)
		#

	les fichiers précedemment en rouge sont maintenant en vert, ce qui indique qu'ils sont prêts à être commités.

6. Commiter cet index

	`git commit -m "xxxxxxxxxxxxxxxxxxxx"`

	en remplacant les xxxxxxxxxxxx par un message expliquant pourquoi vous commitez. 


7. Créez maintenant un sous module maven qui va contenir les **objets 'métier'** de notre projet SupaPuker.
	
	Attention, le groupId du sous-module est le même, mais pas l'artifactId ! Typiquement, pour un projet dont le pom parent a pour artifactId "supapuker", l'artifactId du module enfant contenant les objets du modèle sera "supapuker-model"

	En ligne de commande de l'OS (pas en git bash), se placer dans le répertoire du projet contenant le pom parent (même niveau que le fichier pom.xml) et lancer (remplacer les XXXX par les bonnes valeurs)

	`mvn -DarchetypeGroupId=org.apache.maven.archetypes -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.1 -DarchetypeRepository=http://repo.maven.apache.org/maven2 -DgroupId=XXXX -DartifactId=XXXX -Dversion=1.0-SNAPSHOT -Darchetype.interactive=false --batch-mode archetype:generate`

	Celà va créer un sous-module maven dans le module parent, module dans lequel nous allons coder la couche transverse métier de l'application.

	Examinez le pom.xml du module nouvellement créé ainsi que le pom parent. Chacun 'pointe' sur l'autre (balise 'module', balise 'parent').

	**Vous pouvez ouvrir les modules maven ainsi créés dans votre éditeur de code**.


8. Mettez un peu d'ordre et committez :

	1. Chaque sous-module est destiné à évoluer et suivre la version du parent-pom ainsi que son groupId. Aussi, dans le pom de notre module métier, supprimer la balise <version></version> ainsi que <groupId></groupId>. Ils seront hérités du pom parent.
	2. Supprimez les classes `App.java` et `AppTest.java` générées par maven.
	2. `git status`
		Remarquez les parties "modified" et "untracked"
	3. `git add`
		Quand on ajoute des choses à l'index de commit, il faut préciser quoi. `git add .` indique d'ajouter toutes les modifications du répertoire courant.
	4. `git status`
		Vérifiez ce qui a été ajouté à l'index de commit.
	5. `git commit`
		Attention, souvenez vous que git ne prends pas en compte les répertoires vides !

## Premier développement du jeu
9. Créez un package java (lettres minuscules uniquement) dont le nom est groupId + artifactId en remplacant les - par des points (par exemple, un package com.zonga.supapuker.model)
10. Créez dans ce package une classe PrototypeCardGame qui va nous servir d'ébauche pour vérifier les concepts fonctionnels de base de notre jeu.

	- la classe **`PrototypeCardGame`** doit déclarer une méthode 

			public static void main (String[] args) {
				// your code here
			}

	- la méthode **`main`** doit créer une nouvelle instance de PrototypeCardGame et appeller une méthode d'instance **`runGame()`** qui va lancer la partie.
	- **`runGame()`** doit contenir tout le code du programme pour 'jouer' une partie de façon totallement automatique sans intervention extérieure (les deux joueurs sont gérés par le programme)
		- il n'est pas question de développer une IA (intelligence artificielle) capable d'élaborer des stratégies !
		A chaque tour, prendre une carte au hasard parmis les cartes en main du joueur en guise de carte jouée.


				void runGame () {
					// create cards deck

					// create two players

					// first game turn, both players takes 3 cards

					// while nobody is dead or no more cards in the deck

						// next game turn, each player play a card, and take another one
				}

	
	- **`runGame()`** doit afficher dans la console le nom du gagnant et son nombre de points à la fin de la partie.
		- pour logger, utilisez une librairies de logging comme slf4j. Ajoutez les dépendences nécessaires au pom de votre projet pour pouvoir utiliser cette librairie dans votre code :

				<!-- SLF4J : API de logging, à utiliser dans votre code. -->
				<dependency>
					<groupId>org.slf4j</groupId>
					<artifactId>slf4j-api</artifactId>
					<version>1.6.5</version>
				</dependency>
				<!-- Implementation de l'API (détectée automatiquement par SLF4j) -->
				<dependency>
					<groupId>ch.qos.logback</groupId>
					<artifactId>logback-classic</artifactId>
					<version>1.0.6</version>
				</dependency> 


	- **Contraintes** : 
		- il s'agit d'une ébauche du jeu dont l'objectif est de poser les bases de l'algorithme et d'obtenir un code fonctionnel. NE PAS créer une hiérarchie de classes complexes et de nombreuses méthodes : faire du procédural avec une classe ou deux (`Player` et `Card` peuvent suffire).
		- toutes les classes doivent être dans le même package pour simplifier le refactoring futur (pensez aussi a utiliser les modificateurs de visibilité à bon escient : public, package, protected, private...).

	Pour executer le jeu, vous pouvez utilisez votre IDE (clic-droit sur la classe PrototypeCardGame, run) ou la commande maven :

		mvn exec:java -Dexec.mainClass="com.zonga.supapuker.model.PrototypeCardGame"

	en précisant le bon nom complet de votre classe java.

10. Une fois votre programme fonctionnel, comparez le à celui de la version taggée "vstep02".

	Pour examiner cette version en local sur votre machine, commencez par cloner le repository de ce projet (si ce n'est déjà fait) en git bash :

	`git clone https://snichele@bitbucket.org/snichele/cardgame-project.git`

	Ensuite, checkoutez la version taggée avec la commande git

	`git checkout vstep02`

	- Actuellement, cette solution présente plusieurs défauts :
		- les mécanismes objets sont à peine mis en oeuvre (classes, héritage, design patterns...)
		- les concepts "de base" du génie logiciel ne sont pas mis en oeuvre :
			- Single Responsibility Principle
			- Open Closed Principle
			- Liskow Substitution Principle
			- Dependency Inversion Principle
			- Interface Segregation Principle
			- Law of Demeter
			- Reuse-Release Equivalency Principle
			- Common Closure Principle
			- Common Reuse Principle
			- Acyclic Dependencies Principle
			- The Stable Dependencies Principle
			- Stable Abstractions Principle

		- la lisibilité du code - et donc son évolutivité) et sa maintenabilité - n'est pas optimale 
			- code trop procédural 
			- trop  de copier/coller (beaucoup de duplication de code !)
			- le flot du programme est globalement facile à suivre mais il mélange plusieurs niveau d'abstraction
				- sans les commentaires, il n'est pas forcément évident de distinguer chaque étape 'logique' de déroulement de la partie
				- si les variables étaient nommées de façon moins claire, la difficulté de compréhension augmenterait de façon substantielle
		- le code n'est pas testable unitairement pour plusieurs raisons :
			- la fonction runGame() encapsule toutes les variables et tous les algorithmes de résolution du jeu
			- la fonction runGame() ne reçoit aucun paramètres et contient l'initialisation de l'ensemble des objets du problème
			- les algorithmes contiennent du hasard (fonction random, suffle...)
		- impossible d'ajuster les rêgles du jeu sans modifier la classe principale du programme.

	**Cette première version fonctionelle du programme a permit de "tester" le besoin et de poser les bases des concepts, on pourrait donc décider que le développement est "terminé"**. 

	Mais **du fait des défauts précédents**, il ne l'est est en réalité **pas** ! Il est **absolument nécessaire de corriger les points précédents** pour en faire un programme
	de qualité professionelle et dépasser le stade du prototype.

## Refactoring du code pour rendre le code testable, propre, évolutif et réutilisable.	

Nous allons donc pour l'améliorer procéder, étape par étape, a un refactoring progressif qui va le remettre "sur les rails" de la qualité et des bonnes pratiques.

Chaque amélioration est commitée et est checkoutable en git bash pour suivre l'évolution des modifications. Votre éditeur de code peut vous proposer aussi de visualiser l'historique des commits et de surligner les différences visuellement, ce qui peut se révéler très utile pour la compréhension.

1. commit 6cfca98 (`git checkout 6cfca98`)

	Clarification du code...

	- Introduction d'une enumeration java5 `BasicCardColor` pour les couleurs des cartes
	- Remontée de variables locales au niveau de la classe
	- La création des cartes utilisées par le jeu est isolée dans une fonction dédié
	- Cette fonction est appellée dans le constructeur
	- Diverses fonction utilitaires privées viennent 
		- supprimer la duplication de code à la création du jeux de carte e
		- isoler certains morceaux de code (mélange des cartes, empilage des cartes via une classe Deque) pour améliorer la sémantique et aligner les niveaux d'abstraction de l'agorithme principal.
	
	Pour effectuer les modification, pas de copier coller : utilisation des fonctions de refactoring de l'éditeur de code qui propose normallement un certain nombre
	de facilités **indispensables** pour être **efficace** et éviter les **erreurs de copier/coller**.

2. commit 6ec73e5 (`git checkout 6ec73e5`)

	Clarification du code et premiers pas vers la testabilité.

	- `Card`, `Player` sont remontées au niveau du package, au même niveau que la classe du jeu.
		- Notez la visibilité des classes, de type 'package'. Afin de ne pas offrir prématuremment aux développeurs des autres couches de l'application des classes qui peuvent ne pas leur être utiles, nous limitons leur usage au package de la classe `PrototypeCardGame` qui est leur seul cliente.
	- Les joueurs sont passés en paramètres au constructeur de la classe du jeu pour faire varier ce paramètre lors des tests
		- notez un bug dans la méthode 'runGame()' ou ces valeurs sont écrasées...
	- Suppression de la méthode main qui était utilisée comme substitut de test automatisé invoquable manuellement
	- Introduction d'une classe de test unitaire `PrototypeCardGameTest`. Pour l'instant, à part executer le jeu à chaque build maven, cette classe de test
	ne teste pas grand chose car le code n'est pas encore testable !

	A partir de ce commit, executez la commande `mvn install sonar:sonar` (ligne de commande de l'OS) et examinez le résultat.
	- Remarquez le taux élevé de couverture de test. Pourtant, notre test unitaire ne teste rien du tout ! Quel leçon en tirer ?

3. commit 4b451b5 (`git checkout 4b451b5`)

	Amélioration de la sémantique et application du Single Responsibility Principle

	- La collection de carte semble être un concept récurrent dans le jeu et, suite au refactoring précédent, semble être associé à quelques fonctions utilitaires (initialisation, mélange, empillage...). Aussi, le concept de 'Jeux de carte', munis de ces reponsabilités, devient évident. La création de la classe `Deck` porte maintenant ce concept.
	- Déplacement des méthodes associées au concept de Deck dans celui-ci.
	- Le Deck est passé en paramètre au constructeur de la classe du jeu pour faire varier ce paramètre lors des tests
	- Le test unitaire crée maintenant lui-même le deck de cartes initial.

	Remarquez plusieurs choses dans la classe Deck :
		- Le commentaire de la méthode `newStackFromCards()` est-il vraiment utile ?
		- La visibilité de la classe Deck est elle bien choisie ?
		- Remarquez que la méthode `shuffle()` agit sur le deck et renvoit l'objet Deck : c'est une pratique courante permettant le **chainage de méthode** utilitaires (http://en.wikipedia.org/wiki/Fluent_interface)
		- Remarquez que le getter de la collection de cartes sous-jacente renvoit une collection non modifiable : c'est une bonne pratique de sécurité qui évite que n'importe quel code ne modifie le contenu de la collection (responsabilité de la classe Deck).

	Executez la commande `mvn install sonar:sonar` (ligne de commande de l'OS) et examinez le résultat.
	- Remarquez les bugs 'Major' : Sonar détecte quelques bugs potentiels dans le code (notamment la remarque faite lors de l'analyse du commit précédent) !

4. commit a78f376 (`git checkout a78f376`) 

	Amélioration de la sémantique, des responsabilités des classes et de l'extensibilité du programme.

	- Nous avions déplacé la création du Deck de test dans le test unitaire. Cependant, le deck de test fait aussi partie de l'énoncé de notre jeu. Il a donc sa place dans le code des classes du projet ! Le concept de Deck est une chose, le Deck spécifique à notre jeu est une version particulière de Deck pré-initialisé. Nous avons choisit de créer une sous classe de Deck, `PrototypeGameDeck` pour porter ce concept (d'autres solutions étaient possibles).
	- L'introduction de cette classe, visant à implémenter un aspect spécifique du jeu (un jeu de carte spécifique) par dessus un aspect générique de celui-ci (un jeu de carte) nous indique qu'il serait temps et pertinent d'appliquer le même principe a certains autres concepts. `CardColor` se spécifie, via une classe `PrototypeGameCardColor`.

	Executez la commande `mvn install sonar:sonar` (ligne de commande de l'OS) et examinez le résultat.
	- Remarquez que la complexité diminue. C'est un indicateur important !!!

5. commit 38e2e08

	Ce commit corrige le bug d'affectation des joueurs, faisant disparaitre la moitié des points bloquants dans l'analyse Sonar.

6. commit 6b272e1

	Commit important : un refactoring, utilisant le design pattern Strategy, a été effectué.

	- Certaines rêgles du tirage de carte ont été extraites de l'agorithme principal. Dans un langage de programmation fonctionelle, il aurait été facile de les rendres 'dynamiques' en utilisant le passage de fonction à d'autres fonctions. En Java, on ne peut le faire : aussi, des classes supplémentaires viennent encapsuler la logique de tirage de carte et de choix de la carte à jouer.
	- Remarquer l'utilisation d'interface générique, et de classes spécifiques les implémentants.
	- Remarque le code du test unitaire : le test 'construit' le comportement du programme de façon dynamique en choisissant les stratégies !

	Ce refactoring ne rend cependant pas notre application plus testable, mais il le rend plus évolutif en appliquant plusieurs concepts cités plus haut :
		- Open Closed Principle
		- Liskow Substitution Principle
		- Dependency Inversion Principle
	
7. commit  9847447

	Ce commit rend enfin l'application testable en isolant la partie "random" du code et utilise les mécanismes objet de surcharge de fonction pour supprimer le hasard lors des tests.

	- Remarquer la classe interne `UnshufflablePrototypeGameDeck` dans la classe `PrototypeGameDeck` et la modification du test unitaire en conséquence.

	Une analyse Sonar nous révèle que :
	- la complexité globale à diminué
	- la couverture de test a diminué (la suppression du hasard lors du test fait que moins de code est maintenant testé)

8. commit a1dc1fb

	Le test unitaire a enfin été etoffé pour couvrir plusieurs cas de figure. La couverture de test est remontée mais n'est pas encore parfaite : en effet, tous les cas n'ont pas été testés. Pouvez vous devinez lesquel en vous basant sur les noms des méthodes dans le test unitaire ?

LINKS

http://marklodato.github.com/visual-git-guide/index-en.html



