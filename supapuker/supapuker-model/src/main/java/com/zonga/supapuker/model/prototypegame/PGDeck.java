package com.zonga.supapuker.model.prototypegame;

import com.zonga.supapuker.model.Card;
import com.zonga.supapuker.model.Deck;

/**
 * Create an already initialized Deck of cards for the PrototypeGame.
 *
 * @author snichele
 */
class PGDeck extends Deck {

    public PGDeck() {
        initializeTemplateDeck();
    }

    private void initializeTemplateDeck() {

        addOneCardInBlueAndRedWithValue(1, 4);
        addOneCardInBlueAndRedWithValue(2, 3);
        addOneCardInBlueAndRedWithValue(3, 2);
        addOneCardInBlueAndRedOfEachValues(4, 5, 6, 7, 8, 9, 10);
    }

    private void addOneCardInBlueAndRedWithValue(int value, int howManyTimes) {
        for (int i = 0; i < howManyTimes; i++) {
            add(new Card(PGCardColor.BLUE, value));
            add(new Card(PGCardColor.RED, value));
        }
    }

    private void addOneCardInBlueAndRedOfEachValues(int... values) {
        for (int i : values) {
            add(new Card(PGCardColor.BLUE, i));
            add(new Card(PGCardColor.RED, i));
        }

    }

    /**
     * This class provides an unshufflable game deck.
     */
    public static class UnshufflablePrototypeGameDeck extends PGDeck {

        @Override
        /**
         * Override 'shuffle()' to prevent shuffling of the deck that prevents
         * unit test checks by introducing randomness !
         */
        public PGDeck shuffle() {
            return this;
        }
    }
}
