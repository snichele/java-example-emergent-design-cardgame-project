package com.zonga.supapuker.model;

/**
 * Immutable Card class.
 *
 * @author snichele
 */
public class Card {

    private CardColor color;
    private int value;

    public Card(CardColor color, int value) {
        this.color = color;
        this.value = value;
    }

    public CardColor getColor() {
        return color;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Card{" + "color=" + color + ", value=" + value + '}';
    }
}
