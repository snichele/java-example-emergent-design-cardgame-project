package com.zonga.supapuker.model.prototypegame;

import com.zonga.supapuker.model.Card;
import com.zonga.supapuker.model.CardDrawStrategy;
import com.zonga.supapuker.model.CardPlayStrategy;
import com.zonga.supapuker.model.Deck;
import com.zonga.supapuker.model.Player;
import com.zonga.supapuker.model.TwoCardsConfrontationResolution;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A Prototype Card game to test ideas.
 *
 * @author snichele
 */
class PrototypeCardGame {

    private static final Logger LOGGER = LoggerFactory.getLogger(PrototypeCardGame.class);
    private Deck templateDeck;
    private Deque<Card> stackedCards;
    private CardDrawStrategy firstTurnCardDrawStrategy;
    private CardDrawStrategy inGameTurnCardDrawStrategy;
    private CardPlayStrategy inGameCardPlayStrategy;
    private TwoCardsConfrontationResolution cardsConfrontationResolution;
    private Player player1;
    private Player player2;
    private Player winner;
    private Player looser;
    private boolean tieGame;

    PrototypeCardGame(Player player1, Player player2, Deck deckToUse, CardDrawStrategy firstTurnCardDrawStrategy, CardDrawStrategy inGameTurnCardDrawStrategy, CardPlayStrategy inGameCardPlayStrategy, TwoCardsConfrontationResolution cardsConfrontationResolution) {
        templateDeck = deckToUse;
        this.player1 = player1;
        this.player2 = player2;
        this.firstTurnCardDrawStrategy = firstTurnCardDrawStrategy;
        this.inGameTurnCardDrawStrategy = inGameTurnCardDrawStrategy;
        this.inGameCardPlayStrategy = inGameCardPlayStrategy;
        this.cardsConfrontationResolution = cardsConfrontationResolution;
    }

    void runGame() {
        shuffleDeckAndStackCards();

        playersFillTheirHands();

        int turnCount = 0;
        Card[] cardsPlayed;
        while (gameIsNotFinished()) {
            turnCount++;

            cardsPlayed = playersChooseACard();

            resolveTurn(cardsPlayed);

            declareWinnerByCheckForDeads();

            if (gameIsNotFinished() && thereAreCardsLeft()) {
                playersTakeAnotherCard();
            } else {
                declareWinnerByComparingLifeValues();
            }
        }
        logGameResult(turnCount);
    }

    Player getWinner() {
        return winner;
    }

    Player getLooser() {
        return looser;
    }

    Deque<Card> getStackedCards() {
        return stackedCards;
    }

    private void declareWinnerByCheckForDeads() {
        if (player1.isDead() && player2.isDead()) {
            LOGGER.info(" TIE !");
            tieGame = true;
        } else if (player1.isDead()) {
            winner = player2;
            looser = player1;
        } else if (player2.isDead()) {
            winner = player1;
            looser = player2;
        } else {
            LOGGER.debug("No dead player, proceed to next turn...");
        }
    }

    private boolean declareWinnerByComparingLifeValues() {
        LOGGER.debug("Stack is empty, checking winner !");
        if (player1.getLife() == player2.getLife()) {
            LOGGER.info(" TIE !");
            tieGame = true;
        } else if (player1.getLife() < player2.getLife()) {
            winner = player2;
            looser = player1;
        } else if (player1.getLife() > player2.getLife()) {
            winner = player1;
            looser = player2;
        }
        return false;
    }

    private boolean gameIsNotFinished() {
        return !tieGame && winner == null && looser == null;
    }

    private void shuffleDeckAndStackCards() {
        stackedCards = templateDeck.shuffle().newStackFromCards();
    }

    private void playersFillTheirHands() {
        firstTurnCardDrawStrategy.draws(player1, player2, stackedCards);
    }

    private Card[] playersChooseACard() {
        return inGameCardPlayStrategy.chooseOneCardForEachPlayer(player1, player2);
    }

    private void resolveTurn(Card[] cardsPlayed) {
        cardsConfrontationResolution.resolve(cardsPlayed[0], cardsPlayed[1], player1, player2);
    }

    private boolean thereAreCardsLeft() {
        return !stackedCards.isEmpty();
    }

    private void playersTakeAnotherCard() {
        LOGGER.debug("Stack is not empty, next turn !");
        inGameTurnCardDrawStrategy.draws(player1, player2, stackedCards);
    }

    public boolean isTieGame() {
        return tieGame;
    }

    private void logGameResult(int turnCount) {
        LOGGER.info(" Game was resolved in {} turns.", turnCount);
        LOGGER.info(" Winner is {}", winner);
        LOGGER.info(" Looser is {}", looser);
    }
}