package com.zonga.supapuker.model;

import java.util.Deque;

/**
 * @author snichele
 */
public interface CardDrawStrategy {

    /**
     * Pop cards from the stack (will modify the stack) and give them to the players according to the following rules :
     * - each player pop a card in turn
     * - each player takes 'howManyCardsDrawnByEachPlayer' cards
     *
     * @param player1
     * @param player2
     * @param fromCards
     */
    void draws(Player player1, Player player2, Deque<Card> fromCards);
}
