package com.zonga.supapuker.model.prototypegame;

import com.zonga.supapuker.model.Card;
import com.zonga.supapuker.model.CardDrawStrategy;
import com.zonga.supapuker.model.Player;
import java.util.Deque;

/**
 * How will the first cards drawing (first turn) of the game will be resolved.
 *
 * @author Administrateur
 */
class InGameTurnCardDrawStrategy implements CardDrawStrategy {

    private int p1CardPlayedIndex = 0;
    private int p2CardPlayedIndex = 0;

    /**
     * Pop cards from the stack (will modify the stack) and give them to the
     * players according to the following rules : 
     * - each player pop a card in turn and replace his pXCardPlayedIndex
     *
     * @param player1
     * @param player2
     * @param fromCards
     */
    @Override
    public void draws(Player player1, Player player2, Deque<Card> fromCards) {
        player1.getCards()[p1CardPlayedIndex] = fromCards.pop();
        player2.getCards()[p2CardPlayedIndex] = fromCards.pop();

    }
}
