package com.zonga.supapuker.model.prototypegame;

import com.zonga.supapuker.model.Card;
import com.zonga.supapuker.model.Player;
import com.zonga.supapuker.model.TwoCardsConfrontationResolution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static com.zonga.supapuker.model.prototypegame.PGCardColor.*;

/**
 *
 * @author snichele
 */
class CardsConfrontationResolution implements TwoCardsConfrontationResolution {

    private static final Logger LOGGER = LoggerFactory.getLogger(CardsConfrontationResolution.class);

    @Override
    public void resolve(Card player1PlayedCard, Card player2PlayedCard, Player player1, Player player2) {
        LOGGER.debug("Player {} played card {}", player1, player1PlayedCard);
        LOGGER.debug("Player {} played card {}", player2, player2PlayedCard);
        int p1Damage;
        int p2Damage;
        // card comparison and damage dealt
        if (RED.equals(player2PlayedCard.getColor()) && RED.equals(player1PlayedCard.getColor())) {
            p1Damage = player2PlayedCard.getValue();
            p2Damage = player1PlayedCard.getValue();
        } else if (BLUE.equals(player2PlayedCard.getColor()) && BLUE.equals(player1PlayedCard.getColor())) {
            p1Damage = 0;
            p2Damage = 0;
        } else if (RED.equals(player2PlayedCard.getColor()) && BLUE.equals(player1PlayedCard.getColor())) {
            p1Damage = player2PlayedCard.getValue() - player1PlayedCard.getValue();
            p2Damage = 0;
        } else {
            p2Damage = player1PlayedCard.getValue() - player2PlayedCard.getValue();
            p1Damage = 0;
        }
        LOGGER.debug(" Player {} damage are {} ", player1, p1Damage);
        LOGGER.debug(" Player {} damage are {} ", player2, p2Damage);
        if (p1Damage < 0) {
            p1Damage = 0;
        }
        if (p2Damage < 0) {
            p2Damage = 0;
        }
        player1.takeDamage(p1Damage);
        player2.takeDamage(p2Damage);
    }
}
