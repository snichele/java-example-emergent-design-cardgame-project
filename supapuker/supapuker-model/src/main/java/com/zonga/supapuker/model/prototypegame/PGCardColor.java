package com.zonga.supapuker.model.prototypegame;

import com.zonga.supapuker.model.CardColor;

/**
 *
 * @author snichele
 */
 enum PGCardColor implements CardColor {
    RED, BLUE
    
}
