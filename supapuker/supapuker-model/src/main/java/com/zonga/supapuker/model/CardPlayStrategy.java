package com.zonga.supapuker.model;

/**
 *
 * @author snichele
 */
public interface CardPlayStrategy {

    /**
     * Play cards from players hands.
     *
     * @param player1
     * @param player2
     * @return an array of the two played cards - index 0 is player 1 card and
     * index 1 is player two card.
     */
    Card[] chooseOneCardForEachPlayer(Player player1, Player player2);
}
