package com.zonga.supapuker.model;

/**
 *
 * @author snichele
 */
public class Player {

    private String name;
    private int life;
    private Card[] cardsInHand;

    public Player(String name, int startLife, int maxCardsInHands) {
        this.name = name;
        this.life = startLife;
        cardsInHand = new Card[maxCardsInHands];
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLife() {
        return life;
    }

    public boolean isDead() {
        return life <= 0;
    }

    public Player takeDamage(int lost) {
        if (lost < 0) {
            throw new IllegalArgumentException("Cannot take negative damages !");
        }
        life -= lost;
        return this;
    }

    public void setCardInHandAtPosition(Card c, int position) {
        cardsInHand[position] = c;
    }

    public Card[] getCards() {
        return cardsInHand.clone();
    }

    @Override
    public String toString() {
        return "Player{" + "name=" + name + ", life=" + life + "}";
    }
}
