package com.zonga.supapuker.model;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;

/**
 *
 * @author snichele
 */
public class Deck {

    private Collection<Card> cards = new ArrayList<Card>();

    public Deck() {
    }

    public void add(Card card) {
        if (card == null) {
            throw new IllegalArgumentException("Card to add cannot be null !");
        }
        cards.add(card);
    }

    public Deck shuffle() {
        Collections.shuffle((ArrayList) cards);
        return this;
    }
    
    public Deque<Card> newStackFromCards(){
        final ArrayDeque<Card> arrayDeque = new ArrayDeque<Card>();
        arrayDeque.addAll(getCards());
        return arrayDeque;        
    }

    Collection<Card> getCards() {
        return Collections.unmodifiableCollection(cards);
    }
}