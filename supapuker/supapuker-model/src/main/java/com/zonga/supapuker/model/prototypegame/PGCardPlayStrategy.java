package com.zonga.supapuker.model.prototypegame;

import com.zonga.supapuker.model.Card;
import com.zonga.supapuker.model.CardPlayStrategy;
import com.zonga.supapuker.model.Player;

/**
 *
 * @author Administrateur
 */
class PGCardPlayStrategy implements CardPlayStrategy {

    private int cardIndexPlayedByPlayer1 = 0;
    private int cardIndexPlayedByPlayer2 = 0;
    
    @Override
    public Card[] chooseOneCardForEachPlayer(Player player1, Player player2) {
        return new Card[]{player1.getCards()[cardIndexPlayedByPlayer1],player2.getCards()[cardIndexPlayedByPlayer2]};
    }
    
}
