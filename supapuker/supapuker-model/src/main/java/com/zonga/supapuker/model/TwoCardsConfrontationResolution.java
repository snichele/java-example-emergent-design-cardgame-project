package com.zonga.supapuker.model;

/**
 *
 * @author snichele
 */
public interface TwoCardsConfrontationResolution {

    void resolve(Card player1PlayedCard, Card player2PlayedCard, Player player1, Player player2);
    
}
