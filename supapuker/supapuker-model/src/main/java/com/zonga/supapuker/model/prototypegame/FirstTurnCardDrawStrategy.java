package com.zonga.supapuker.model.prototypegame;

import com.zonga.supapuker.model.Card;
import com.zonga.supapuker.model.Player;
import java.util.Deque;

/**
 * How will the first cards drawing (first turn) of the game will be resolved.
 * 
 * @author Administrateur
 */
class FirstTurnCardDrawStrategy implements com.zonga.supapuker.model.CardDrawStrategy {

    private int howManyCardsDrawnByEachPlayer;

    public FirstTurnCardDrawStrategy(int howManyCardsDrawnByEachPlayer) {
        this.howManyCardsDrawnByEachPlayer = howManyCardsDrawnByEachPlayer;
    }

    /**
     * Pop cards from the stack (will modify the stack) and give
     * them to the players according to the following rules :
     * - each player pop a card in turn
     * - each player takes 'howManyCardsDrawnByEachPlayer' cards
     * 
     * @param player1
     * @param player2
     * @param fromCards 
     */
    @Override
    public void draws(Player player1, Player player2, Deque<Card> fromCards) {
        for (int i = 0; i < howManyCardsDrawnByEachPlayer; i++) {
            player1.setCardInHandAtPosition(fromCards.pop(),i);
            player2.setCardInHandAtPosition(fromCards.pop(),i);
        }
    }
}
