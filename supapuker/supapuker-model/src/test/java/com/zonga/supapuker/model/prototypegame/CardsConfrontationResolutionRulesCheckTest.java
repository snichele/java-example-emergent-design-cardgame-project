package com.zonga.supapuker.model.prototypegame;

import com.zonga.supapuker.model.Card;
import com.zonga.supapuker.model.Player;
import org.junit.Test;
import static org.mockito.Mockito.*;
import static com.zonga.supapuker.model.prototypegame.PGCardColor.*;

/**
 *
 * @author snichele
 */
public class CardsConfrontationResolutionRulesCheckTest {

    @Test
    public void blue_versus_blue_same_value() {
        check(BLUE, 5, BLUE, 5, 0, 0);
    }

    @Test
    public void red_versus_red_same_value() {
        check(RED, 5, RED, 5, 5, 5);
    }

    @Test
    public void red_versus_blue_same_value() {
        check(RED, 5, BLUE, 5, 0, 0);
    }

    @Test
    public void red_versus_blue_notsame_value_nodamage() {
        check(RED, 5, BLUE, 10, 0, 0);
    }

    @Test
    public void blue_versus_red_notsame_value_nodamage() {
        check(BLUE, 10, RED, 5, 0, 0);
    }

    @Test
    public void red_versus_blue_notsame_value_player2TakesDamage() {
        check(RED, 10, BLUE, 5, 0, 5);
    }

    @Test
    public void red_versus_blue_notsame_value_player1TakesDamage() {
        check(BLUE, 5, RED, 10, 5, 0);
    }

    private void check(final PGCardColor c1Color, final int c1Value, final PGCardColor c2Color, final int c2Value, final int p1DamagesTaken, final int p2DamagesTaken) {
        Player p1 = mock(Player.class);
        Player p2 = mock(Player.class);
        Card c1 = mock(Card.class);
        when(c1.getColor()).thenReturn(c1Color);
        when(c1.getValue()).thenReturn(c1Value);

        Card c2 = mock(Card.class);
        when(c2.getColor()).thenReturn(c2Color);
        when(c2.getValue()).thenReturn(c2Value);

        CardsConfrontationResolution pgccr = new CardsConfrontationResolution();
        pgccr.resolve(c1, c2, p1, p2);

        verify(p1).takeDamage(p1DamagesTaken);
        verify(p2).takeDamage(p2DamagesTaken);
    }
}
