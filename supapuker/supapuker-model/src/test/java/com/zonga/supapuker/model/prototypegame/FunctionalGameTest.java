package com.zonga.supapuker.model.prototypegame;

import com.zonga.supapuker.model.Card;
import com.zonga.supapuker.model.Deck;
import com.zonga.supapuker.model.Player;
import static com.zonga.supapuker.model.prototypegame.PGCardColor.BLUE;
import static com.zonga.supapuker.model.prototypegame.PGCardColor.RED;
import java.util.ArrayDeque;
import java.util.Deque;
import static org.fest.assertions.Assertions.assertThat;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author snichele
 */
public class FunctionalGameTest {

    private Deck templateDeck;
    private Player player1;
    private Player player2;

    @Before
    public void initPlayers() {
        player1 = new Player("Player 1", 20, 3);
        player2 = new Player("Player 2", 20, 3);
    }

    @Test
    public void test_rules_TIE_game_by_empty_deck() {
        templateDeck = new com.zonga.supapuker.model.prototypegame.PGDeck.UnshufflablePrototypeGameDeck();
        PrototypeCardGame pcg = newCardGameWithDefaultsFixtures();
        pcg.runGame();
        assertThat(pcg.isTieGame()).isTrue();
        assertThat(pcg.getWinner()).isNull();
        assertThat(pcg.getLooser()).isNull();
        assertThat(pcg.getStackedCards()).isEmpty();
    }

    @Test
    public void test_rules_winner_player1_killing_player2_before_empty_stack() {
        templateDeck = new Deck() {
            @Override
            public Deque<Card> newStackFromCards() {
                final ArrayDeque<Card> arrayDeque = new ArrayDeque<Card>();
                arrayDeque.add(new Card(RED, 10));
                arrayDeque.add(new Card(BLUE, 1));
                arrayDeque.add(new Card(RED, 9));
                arrayDeque.add(new Card(BLUE, 1));
                arrayDeque.add(new Card(RED, 8));
                arrayDeque.add(new Card(BLUE, 1));
                arrayDeque.add(new Card(RED, 7));
                arrayDeque.add(new Card(BLUE, 1));
                arrayDeque.add(new Card(RED, 6));
                arrayDeque.add(new Card(BLUE, 2));
                arrayDeque.add(new Card(RED, 5));
                arrayDeque.add(new Card(BLUE, 2));
                return arrayDeque;
            }
        };
        PrototypeCardGame pcg = newCardGameWithDefaultsFixtures();
        pcg.runGame();
        assertThat(pcg.isTieGame()).isFalse();
        assertThat(pcg.getWinner()).isEqualTo(player1);
        assertThat(pcg.getWinner().getLife()).isEqualTo(20);
        assertThat(pcg.getLooser()).isEqualTo(player2);
        assertThat(pcg.getLooser().getLife()).isEqualTo(-7);
        assertThat(pcg.getStackedCards()).isNotEmpty();
    }

    @Test
    public void test_rules_winner_player2_killing_player1_before_empty_stack() {
        templateDeck = new Deck() {
            @Override
            public Deque<Card> newStackFromCards() {
                final ArrayDeque<Card> arrayDeque = new ArrayDeque<Card>();
                arrayDeque.add(new Card(BLUE, 1));
                arrayDeque.add(new Card(RED, 10));
                arrayDeque.add(new Card(BLUE, 1));
                arrayDeque.add(new Card(RED, 9));
                arrayDeque.add(new Card(BLUE, 1));
                arrayDeque.add(new Card(RED, 8));
                arrayDeque.add(new Card(BLUE, 1));
                arrayDeque.add(new Card(RED, 7));
                arrayDeque.add(new Card(BLUE, 2));
                arrayDeque.add(new Card(RED, 6));
                arrayDeque.add(new Card(BLUE, 2));
                arrayDeque.add(new Card(RED, 5));
                return arrayDeque;
            }
        };
        PrototypeCardGame pcg = newCardGameWithDefaultsFixtures();
        pcg.runGame();
        assertThat(pcg.isTieGame()).isFalse();
        assertThat(pcg.getWinner()).isEqualTo(player2);
        assertThat(pcg.getLooser()).isEqualTo(player1);
        assertThat(pcg.getWinner().getLife()).isEqualTo(20);
        assertThat(pcg.getLooser().getLife()).isEqualTo(-7);
        assertThat(pcg.getStackedCards()).isNotEmpty();
    }

    private PrototypeCardGame newCardGameWithDefaultsFixtures() {
        return new PrototypeCardGame(
                player1,
                player2,
                templateDeck,
                new FirstTurnCardDrawStrategy(3),
                new InGameTurnCardDrawStrategy(),
                new PGCardPlayStrategy(),
                new CardsConfrontationResolution());
    }
}
